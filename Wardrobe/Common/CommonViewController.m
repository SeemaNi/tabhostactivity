//
//  CommonViewController.m
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    [self.view setBackgroundColor:[UIColor colorWithRed:257/255.0f green:237/255.0f blue:223/255.0f alpha:1.0]];
    UIImage *logoBarImage=[UIImage imageNamed:@"logo_Bar"];
    UIImage *sideMenuImage=[UIImage imageNamed:@"menu"];
    UIImage *backImge=[UIImage imageNamed:@"back"];
    UIImage *bottomImage=[UIImage imageNamed:@"bottomBar"];
    self.bottomBar =[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-bottomImage.size.height, bottomImage.size.width, bottomImage.size.height)];
    [self.bottomBar setImage:bottomImage];
    [self.view addSubview:self.bottomBar];
    
    self.backButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setImage:backImge forState:UIControlStateNormal];
    [self.backButton setFrame:CGRectMake(280, 01, backImge.size.width, backImge.size.height)];
    
    [self.backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.sideMenuButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [self.sideMenuButton setImage:sideMenuImage forState:UIControlStateNormal];
    [self.sideMenuButton addTarget:self action:@selector(sideMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.sideMenuButton setFrame:CGRectMake(0, 01, sideMenuImage.size.width, sideMenuImage.size.height)];
    self.topLogoBar =[[UIImageView alloc]initWithFrame:CGRectMake(0, 20, logoBarImage.size.width, logoBarImage.size.height)];
    [self.topLogoBar setImage:logoBarImage];
    [self.topLogoBar addSubview:self.sideMenuButton];
    [self.topLogoBar addSubview:self.backButton];
    [self.topLogoBar setUserInteractionEnabled:YES];
    [self.view addSubview:self.topLogoBar];
}

-(IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)sideMenuButtonAction:(id)sender
{
    NSLog(@"Side menu button action");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
