//
//  ViewController.m
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"loginhome"]]];
    [self addLabelWithFrame:CGRectMake(67.0, 170.0, 100.0, 15.0) tag:1001 title:@"user"];
    [self addLabelWithFrame:CGRectMake(40.0, 200.0, 100.0, 17.0) tag:1002 title:@"password"];
    
    [self addTextFieldWithFrame:CGRectMake(104.0, 170.0, 180.0, 15.0) tag:2001 title:@""];
    [self addTextFieldWithFrame:CGRectMake(104.0, 200.0, 180.0, 15.0) tag:2002 title:@""];
    
    [self addButtonWithFrame:CGRectMake(128.0, 228.0, 60.0, 30.0) tag:3001 title:@"login" image:[UIImage imageNamed:@"loginbutton"]];
    
}


-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    label.font = [UIFont fontWithName:@"Times New Roman" size:14];
    [label setTextColor: [UIColor blueColor]];
    [self.view addSubview: label];
}

-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:12];
    tf.backgroundColor=[UIColor whiteColor];
    //tf.text=@"Hello World";
    tf.tag=tag;
    tf.layer.borderColor=[[UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
    [self.view addSubview:tf];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:59.0/255 alpha:1.0] forState:UIControlStateNormal];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    //button.layer.borderColor=[[UIColor blackColor]CGColor];
    //button.layer.borderWidth= 1.0f;
}

-(IBAction)buttonAction:(id)sender
{
    HomeViewController * home = [[HomeViewController alloc]init];
    [self.navigationController pushViewController:home animated:NO];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
