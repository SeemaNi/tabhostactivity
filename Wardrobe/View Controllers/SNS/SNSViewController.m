//
//  SNSViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "SNSViewController.h"
#import "HomeViewController.h"
#import "CoordiViewController.h"
#import "NewsViewController.h"
#import "SettingsViewController.h"

@interface SNSViewController ()
{
    UIScrollView * scroller;
    NSMutableArray * image_array;
    UIScrollView * scroller1;
}
@end

@implementation SNSViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self addButtonWithFrame:CGRectMake(20.0, 65.0, 42.0, 42.0) tag:1001 title:@"" selected:[UIImage imageNamed:@"popular"]unselected:[UIImage imageNamed:@"popularselected"]];
    [self addButtonWithFrame:CGRectMake(100.0, 65.0, 42.0, 42.0) tag:1002 title:@"" selected:[UIImage imageNamed:@"new"]unselected:[UIImage imageNamed:@"news_selected"]];
    [self addButtonWithFrame:CGRectMake(180.0, 65.0, 42.0, 42.0) tag:1003 title:@"" selected:[UIImage imageNamed:@"taste"]unselected:[UIImage imageNamed:@"tasteselected"]];
    [self addButtonWithFrame:CGRectMake(260.0, 65.0, 42.0, 42.0) tag:1004 title:@"" selected:[UIImage imageNamed:@"follow"]unselected:[UIImage imageNamed:@"followselected"]];
    [self addButtonWithFrame:CGRectMake(118.0, 335.0, 100.0, 25) tag:1005 title:[NSString stringWithFormat:NSLocalizedString(@"Choose Taste", nil)] selected:[UIImage imageNamed:@"choose_taste_button"]unselected:[UIImage imageNamed:@""]];
    
    
    [self addLabelWithFrame:CGRectMake(21.0, 95.0, 80.0, 40.0) tag:2001 title:[NSString stringWithFormat:NSLocalizedString(@"Popular", nil)]BackgroundColor:[UIColor clearColor] textcolor:[UIColor colorWithRed:178.0/255 green:157.0/255 blue:136.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(109.0, 95.0, 40.0, 40.0) tag:2002 title:[NSString stringWithFormat:NSLocalizedString(@"New", nil)]BackgroundColor:[UIColor clearColor] textcolor:[UIColor colorWithRed:178.0/255 green:157.0/255 blue:136.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(188.0, 95.0, 60.0, 40.0) tag:2003 title:[NSString stringWithFormat:NSLocalizedString(@"Taste", nil)]BackgroundColor:[UIColor clearColor] textcolor:[UIColor colorWithRed:178.0/255 green:157.0/255 blue:136.0/255 alpha:1.0]];
    [self addLabelWithFrame:CGRectMake(263.0, 95.0, 60.0, 40.0) tag:2004 title:[NSString stringWithFormat:NSLocalizedString(@"Follow", nil)]BackgroundColor:[UIColor clearColor] textcolor:[UIColor colorWithRed:178.0/255 green:157.0/255 blue:136.0/255 alpha:1.0]];
    
    [self addSeparatorLineWithFrame:CGRectMake(0.0, 130.0, 320.0, 1.0)BGColor:[UIColor colorWithRed:190.0/255 green:155.0/255 blue:129.0/255 alpha:1.0]];
    
    [self.backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

-(void)backButtonAction
{
    UIScrollView *scrollView=(UIScrollView*)[self.view viewWithTag:874];
    if (scrollView) {
        [scrollView removeFromSuperview];
        scrollView=nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title selected:(UIImage *)image unselected:(UIImage *)image1
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor colorWithRed:149.0/255 green:111.0/255 blue:64.0/255 alpha:1.0] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"Times New Roman" size:14];
    [button setTitle:title forState:UIControlStateNormal];
    
    
    UIImage * unselectedimage = image;
    [button setBackgroundImage:unselectedimage forState:UIControlStateNormal];
    
    UIImage * selectedimage = image1;
    [button setBackgroundImage:selectedimage forState:UIControlStateSelected];
        [self.view addSubview:button];
}

-(void)addSeparatorLineWithFrame:(CGRect)frame BGColor:(UIColor *)color
{
    UIView *lineView=[[UIView alloc]initWithFrame:frame];
    [lineView setBackgroundColor:color];
    [self.view addSubview:lineView];
}
-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title BackgroundColor:(UIColor *)BackgroundColor textcolor:(UIColor *)textcolor
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    label.backgroundColor = BackgroundColor ;
    label.font = [UIFont fontWithName:@"Times New Roman" size:14];
    [label setTextColor: textcolor];
    [self.view addSubview: label];
}
-(void)addViewWithFrame:(CGRect)frame bgcolor:(UIColor*)bgcolor
{
    UIView * view = [[UIView alloc]init];
    view.frame = frame;
    view.backgroundColor = bgcolor;
    [self.view addSubview:view];
}

-(IBAction)buttonAction:(UIButton*)sender
{
    if (sender.tag == 1007)
    {
        HomeViewController *myController = (HomeViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:myController animated:NO];
    }
    if (sender.tag ==1004)
    {
        UIView * view = [[UIView alloc]init];
        view.frame = CGRectMake(0.0, 132.0, 320.0, 255.0);
        view.backgroundColor = [UIColor colorWithRed:252.0/255 green:238.0/255 blue:222.0/255 alpha:1.0];
        [self.view addSubview:view];
        
        UIView * viewimgbdr = [[UIView alloc]initWithFrame:CGRectMake(05.0, 07.0, 30.0, 30.0)];
        viewimgbdr.backgroundColor = [UIColor colorWithRed:252.0/255 green:254.0/255 blue:251.0/255 alpha:1.0];
        viewimgbdr.layer.borderColor = [UIColor colorWithRed:199.0/255 green:191.0/255 blue:188.0/255 alpha:1.0].CGColor;
        viewimgbdr.layer.borderWidth = 1.0;
        [view addSubview:viewimgbdr];
        
        UIImageView *img_view = [[UIImageView alloc]init];
        img_view.frame = CGRectMake(07.0, 09.0, 27.0, 27.0);
        img_view.image = [UIImage imageNamed:@"chen"];
        [view addSubview:img_view];
        
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(42.0, 15.0, 60.0, 10.0)];
        label.text = @"user name";
        label.textColor = [UIColor colorWithRed:194.0/255 green:44.0/255 blue:55.0/255 alpha:1.0];
        label.font =[UIFont fontWithName:@"Times New Roman" size:10];
        [view addSubview:label];
        
        UILabel * label1 = [[UILabel alloc]initWithFrame:CGRectMake(42.0, 25.0, 60.0, 10.0)];
        label1.text = @"chetan0910";
        label1.textColor = [UIColor colorWithRed:137.0/255 green:109.0/255 blue:72.0/255 alpha:1.0];
        label1.font =[UIFont fontWithName:@"Times New Roman" size:10];
        [view addSubview:label1];
        
        UIImageView * images = [[UIImageView alloc]init];
       // UILabel * lbl_images;
        
        for(int i=0; i<4; i++)
        {
            float horizontal = 08.0;
            float vertical = 45.0;
            
            images.frame = CGRectMake(horizontal, vertical, 60.0, 46.0);
            images.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"calender_5.jpg"]];
            [view addSubview:images];
            horizontal = horizontal + 80.0 + 12.0;
        }
    }
    if (sender.tag == 1005)
    {
        image_array = [[NSMutableArray alloc]initWithObjects:@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",@"snspopular.jpg",nil];

        [self addScrollView];
        
        [self addImageViewWithFrame:CGRectMake(06.0, 05.0, 51.0, 51.0) image:[UIImage imageNamed:@"snspopular.jpg"]color:[UIColor whiteColor]];
        
        [self addLabelWithFrame:CGRectMake(72.0, 05.0, 60.0, 20.0) title:@"Chetan" font:[UIFont fontWithName:@"Times New Roman" size:10.0] color:[UIColor colorWithRed:153.0/255 green:103.0/255 blue:40.0/255 alpha:1.0]];
        
        [self addLabelWithFrame:CGRectMake(72.0, 35.0, 40.0, 20.0) title:@"Coordi" font:[UIFont fontWithName:@"Times New Roman" size:10.0] color:[UIColor colorWithRed:37.0/255 green:62.0/255 blue:29.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(108.0, 36.0, 40.0, 20.0) title:@"300" font:[UIFont fontWithName:@"Times New Roman" size:10.0] color:[UIColor colorWithRed:66.0/255 green:90.0/255 blue:131.0/255 alpha:1.0]];
        
        
        [self addLabelWithFrame:CGRectMake(142.0, 35.0, 50.0, 20.0) title:@"Followers" font:[UIFont fontWithName:@"Times New Roman" size:10] color:[UIColor colorWithRed:66.0/255 green:62.0/255 blue:29.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(185.0, 36.0, 40.0, 20.0) title:@"10" font:[UIFont fontWithName:@"Times New Roman" size:10.0] color:[UIColor colorWithRed:37.0/255 green:90.0/255 blue:131.0/255 alpha:1.0]];
        
        
        [self addLabelWithFrame:CGRectMake(218.0, 35.0, 70.0, 20.0) title:@"Get Followers" font:[UIFont fontWithName:@"Times New Roman" size:10] color:[UIColor colorWithRed:66.0/255 green:62.0/255 blue:59.0/255 alpha:1.0]];
        [self addLabelWithFrame:CGRectMake(280.0, 36.0, 40.0, 20.0) title:@"100" font:[UIFont fontWithName:@"Times New Roman" size:10.0] color:[UIColor colorWithRed:37.0/255 green:90.0/255 blue:131.0/255 alpha:1.0]];
        
        [self addSeparatorLineOnScrollViewWithFrame:CGRectMake(0.0, 70.0, 320.0, 1.0) BGColor:[UIColor colorWithRed:243.0/255 green:228.0/255 blue:209.0/255 alpha:1.0]];
        NSLog(@"image array count %d",image_array.count);
        
        [self addButtonWithFrame:CGRectMake(276.0, 135.0, 33.0, 12.0) tag:1006 title:@"" selected:[UIImage imageNamed:@"star_rate"] unselected:[UIImage imageNamed:@""]];
        
        UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
        UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
        
        [self addSliderWithFrame:CGRectMake(218.0, 214.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
        
        [self addGridImages];
        
    }
    UIButton *popular = (UIButton *)[self.view viewWithTag:1001];
    UIButton *new=(UIButton*)[self.view viewWithTag:1002];
    UIButton *taste = (UIButton *)[self.view viewWithTag:1003];
    UIButton *follow=(UIButton*)[self.view viewWithTag:1004];
    
    if (sender.tag == 1001)
    {
        [popular setSelected:YES];
        [new setSelected:NO];
        [taste setSelected:NO];
        [follow setSelected:NO];
    }
    if (sender.tag == 1002)
    {
        [popular setSelected:NO];
        [new setSelected:YES];
        [taste setSelected:NO];
        [follow setSelected:NO];
    }
    if (sender.tag == 1003)
    {
        [popular setSelected:NO];
        [new setSelected:NO];
        [taste setSelected:YES];
        [follow setSelected:NO];
    }
    if (sender.tag == 1004)
    {
        [popular setSelected:NO];
        [new setSelected:NO];
        [taste setSelected:NO];
        [follow setSelected:YES];
    }
    if (sender.tag ==1111)
    {
        UIView * view = (UIView *)[self.view viewWithTag:1212];
        if (view)
        {
            return;
        }
        view = [[UIView alloc]init];
        view.frame = CGRectMake(05.0, 170.0, 310.0, 142.0);
        view.backgroundColor = [UIColor colorWithRed:168.0/255 green:138.0/255 blue:111.0/255 alpha:0.9];
        [view setOpaque:YES];
        [view.layer setBorderColor:[UIColor colorWithRed:162.0/255 green:126.0/255 blue:90.0/255 alpha:1.0].CGColor];
        view.tag = 1212;
        [view.layer setBorderWidth:1.0];
        [view.layer setCornerRadius:5.0];
        [self.view addSubview:view];
        
    
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 10.0, 200.0, 20.0)];
        [label setText:@"Are you sure you want to buy???"];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Times New Roman" size:12];
        label.textColor = [UIColor colorWithRed:253.0/255 green:242.0/255 blue:218.0/255 alpha:1.0];
        [view addSubview:label];
        
        UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(87.0, 90.0, 62.0, 23.0)];
        [button addTarget:self
                   action:@selector(buttonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag=1112;
        [button setTitleColor:[UIColor colorWithRed:149.0/255 green:111.0/255 blue:64.0/255 alpha:1.0] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"Times New Roman" size:14];
        [button setTitle:@"Yes" forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"smallbtn1"] forState:UIControlStateNormal];
        [view addSubview:button];
        
        UIButton * button1 = [[UIButton alloc]initWithFrame:CGRectMake(166.0, 90.0, 62.0, 23.0)];
        [button1 addTarget:self
                    action:@selector(buttonAction:)
          forControlEvents:UIControlEventTouchUpInside];
        button1.tag=1112;
        [button1 setTitleColor:[UIColor colorWithRed:149.0/255 green:111.0/255 blue:64.0/255 alpha:1.0] forState:UIControlStateNormal];
        button1.titleLabel.font = [UIFont fontWithName:@"Times New Roman" size:14];
        [button1 setTitle:@"No" forState:UIControlStateNormal];
        [button1 setBackgroundImage:[UIImage imageNamed:@"smallbtn2"] forState:UIControlStateNormal];
        [view addSubview:button1];
    }
    if (sender.tag ==1112)
    {
        UIView * view = (UIView *) [self.view viewWithTag:1212];
        [view removeFromSuperview];
    }
    if (sender.tag == 1113)
    {
        UIView * view = (UIView *) [self.view viewWithTag:1212];
        [view removeFromSuperview];
        
    }

}

-(void)addSeparatorLineOnScrollViewWithFrame:(CGRect)frame BGColor:(UIColor *)color
{
    UIView *lineView=[[UIView alloc]initWithFrame:frame];
    [lineView setBackgroundColor:color];
    [scroller addSubview:lineView];
}

-(void)addScrollView
{
    scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 132.0, 320.0, 255.0)];
    scroller.showsVerticalScrollIndicator = YES;
    scroller.scrollEnabled = YES;
    scroller.delegate = (id)self ;
    scroller.backgroundColor = [UIColor colorWithRed:254.0/255 green:238.0/255 blue:222.0/255 alpha:1.0];
    scroller.userInteractionEnabled = YES;
    NSLog(@"imagearray count %d",image_array.count);
    
    int contentHeight=(image_array.count%4!=0)?(image_array.count/4)+1:image_array.count/4;
    [scroller setContentSize:CGSizeMake(scroller.frame.size.width, contentHeight*93)];
    
    //[scroller setContentSize:CGSizeMake(0.0, 1000)];
    [self.view addSubview:scroller];
}

-(void)addGridImages
{
float horizontal = 10.0;
float vertical = 85.0;
UILabel * lblimages;

for (int i =0; i<[image_array count]; i++)
{
    if ((i%4) ==0 && i!=0)
    {
        horizontal = 10.0;
        vertical = vertical + 58.0 + 22.0;
    }
    UIImageView* grid_images = [[UIImageView alloc]init];
    [grid_images setFrame:CGRectMake(horizontal+1, vertical+3,59.0, 48.0)];
    [grid_images setTag:i];
    [grid_images setImage:[UIImage imageNamed:[image_array objectAtIndex:i]]];

    lblimages = [[UILabel alloc]init];
    [lblimages setFrame:CGRectMake(horizontal+ 0.0, vertical+ 48.0, 60.0, 15.0)];
    [lblimages setTag:i];
    [lblimages setTextAlignment:NSTextAlignmentCenter];
    [lblimages setText:@"Dress"];
    [lblimages setTextColor:[UIColor colorWithRed:111.0/255 green:107.0/255 blue:106.0/255 alpha:1.0]];
    lblimages.font = [UIFont fontWithName:@"Times New Roman" size:10];
    [lblimages setBackgroundColor:[UIColor colorWithRed:248.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
    
    UIView * view_border = [[UIView alloc]initWithFrame:CGRectMake(horizontal-2, vertical, 65.0, 65.0)];
    
    [view_border.layer setShadowColor:[UIColor blackColor].CGColor];
    [[view_border layer]setBackgroundColor:[UIColor colorWithRed:248.0/255 green:244.0/255 blue:243.0/255 alpha:1.0].CGColor];
    [view_border.layer setShadowOpacity:0.8];
    [view_border setTag:343+i];
    [view_border.layer setShadowRadius:3.0];
    [view_border.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [scroller addSubview:view_border];
    
    [scroller addSubview:grid_images];
    [scroller addSubview:lblimages];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOnItemSelection:)];
    [tapGesture setDelegate:(id)self];
    
    [view_border addGestureRecognizer:tapGesture];
    
    horizontal = horizontal + 65.0 + 12.0;
}
}
-(IBAction)actionOnItemSelection:(UIGestureRecognizer*)sender
{
    NSLog(@"itemSlected with tag info %d",sender.view.tag);
  scroller1 = [[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 132.0, 320.0, 255.0)];
    scroller1.showsVerticalScrollIndicator = YES;
    scroller1.scrollEnabled = YES;
    scroller1.delegate = (id)self ;
    scroller1.backgroundColor = [UIColor colorWithRed:254.0/255 green:238.0/255 blue:222.0/255 alpha:1.0];
    scroller1.userInteractionEnabled = YES;
    NSLog(@"imagearray count %d",image_array.count);
    [scroller1 setTag:874];
    int contentHeight=(image_array.count%4!=0)?(image_array.count/4)+1:image_array.count/4;
    [scroller1 setContentSize:CGSizeMake(scroller.frame.size.width, contentHeight*48)];
    [self.view addSubview:scroller1];
    
    UIImage *image=[UIImage imageNamed:@"clickable_item.jpg"];
    UIImageView * imageview = [[UIImageView alloc]init];
    imageview.frame = CGRectMake(60.0, 14.0, 200.0, 180.0);
    imageview.image = image;
    
    UIView * img_view = [[UIView alloc]init];
    img_view.frame = CGRectMake(54.0, 8.0, 212.0, 217.0);
    img_view.backgroundColor = [UIColor colorWithRed:248.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    img_view.layer.shadowColor = [UIColor colorWithRed:211.0 green:206.0/255 blue:202.0/255 alpha:1.0].CGColor;
    [img_view.layer setShadowOpacity:0.8];
    [img_view.layer setShadowRadius:4.0];
    [img_view.layer setShadowOffset:CGSizeMake(0.0, 4.0)];
    [scroller1 addSubview:img_view];
    
    UIButton * img_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [img_btn addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    img_btn.frame = CGRectMake(139.0, 232.0, 50.0, 19.0);
    img_btn.tag=1111;
    [img_btn setTitle:@"Button" forState:UIControlStateNormal];
    img_btn.titleLabel.font = [UIFont fontWithName:@"Times New Roman" size:13];
    [img_btn setTitleColor:[UIColor colorWithRed:150.0/255 green:114.0/255 blue:63.0/255 alpha:1.0] forState:UIControlStateNormal];
    img_btn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"snsbbtn"]];
    [scroller1 addSubview:img_btn];
    
    UIButton * views_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [views_btn addTarget:self
                action:@selector(buttonAction:)
      forControlEvents:UIControlEventTouchUpInside];
    views_btn.frame = CGRectMake(280.0, 226.0, 36.0, 28.0);
    views_btn.tag=2222;
    
    //[views_btn setTitle:@"" forState:UIControlStateNormal];
    views_btn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"viewers"]];
    [scroller1 addSubview:views_btn];
    
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    
    [self addSliderWithFrame:CGRectMake(218.0, 214.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
    
    [self addLabelOnScroller1WithFrame:CGRectMake(58.0, 193.0, 202.0, 30.0) textcolor:[UIColor colorWithRed:65.0/255 green:59.0/255 blue:59.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"Dress" bgcolor:[UIColor colorWithRed:248.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
    
    [self addLabelOnScroller1WithFrame:CGRectMake(60.0, 232.0, 80.0, 20.0) textcolor:[UIColor colorWithRed:65.0/255 green:59.0/255 blue:59.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"abcdefghi" bgcolor:[UIColor clearColor]];
    
    [self addLabelOnScroller1WithFrame:CGRectMake(205.0, 213.0, 60.0, 10.0) textcolor:[UIColor colorWithRed:65.0/255 green:59.0/255 blue:59.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:07] title:@"2013.10.02  16.00" bgcolor:[UIColor clearColor]];
    
    
    [self addLabelOnScroller1WithFrame:CGRectMake(283.0, 230.0, 36.0, 10.0) textcolor:[UIColor whiteColor] font:[UIFont fontWithName:@"Helvetica-Bold" size:13] title:@"500" bgcolor:[UIColor clearColor]];
    [self addLabelOnScroller1WithFrame:CGRectMake(284.0, 240.0, 36.0, 10.0) textcolor:[UIColor whiteColor] font:[UIFont fontWithName:@"Helvetica-Bold" size:07] title:@"Views" bgcolor:[UIColor clearColor]];
    
    [scroller1 addSubview:imageview];

}

-(void)addLabelOnScroller1WithFrame:(CGRect)frame textcolor:(UIColor *)textcolor font:(UIFont *)font title:(NSString *)title bgcolor:(UIColor *) bgcolor
{
    UILabel * label = [[UILabel alloc]init];
    label.frame = frame;
    label.backgroundColor = bgcolor;
    label.text = title;
    label.font = font;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = textcolor;
    [scroller1 addSubview:label];
}


-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}

-(void)addImageViewWithFrame:(CGRect)frame image:(UIImage *)image color:(UIColor *)color
{
    UIImageView * imageview = [[UIImageView alloc]init];
    imageview.frame = frame;
    imageview.image = image;
    imageview.layer.borderColor = color.CGColor;
    imageview.layer.borderWidth = 4.0f;
    imageview.layer.shadowColor =  [UIColor colorWithRed:211.0/255 green:206.0/255 blue:202.0/255 alpha:1.0].CGColor;
    [scroller addSubview:imageview];
}

-(void)addLabelWithFrame:(CGRect)frame title:(NSString *)title font:(UIFont *)font color:(UIColor *)color
{
    UILabel * label  = [[UILabel alloc]init];
    label.frame = frame;
    label.text = title;
    label.font = font;
    label.textColor = color;
    [scroller addSubview:label];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
