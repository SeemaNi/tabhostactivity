//
//  Coordi_Category.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 17/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Coordi_Category.h"
#import "CoordiViewController.h"

@interface Coordi_Category ()
{
   
    UIScrollView *scrolView;
    NSMutableArray *categoryImagesArray;

    
}
@end

@implementation Coordi_Category

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.bottomBar setHidden:NO];

    categoryImagesArray =[NSMutableArray new];
    scrolView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height, self.view.frame.size.width, 330)];
    [scrolView setDelegate:(id)self];
    [scrolView setShowsVerticalScrollIndicator:NO];
    [scrolView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scrolView];
    for (int i=0; i<=43; i++)
    {
        [categoryImagesArray addObject:[UIImage imageNamed:@"hanger_round"]];
    }
    int x=-90,y=10;
    for (int i=0; i<categoryImagesArray.count; i++)
    {
        if (i%3==0  && i!=0)
        {
            y+=110;
            x=15;
        }
        else
            x+=105;
        UIImage *hangerImage=[categoryImagesArray objectAtIndex:i];

        [self addCategoryImage:hangerImage frame:CGRectMake(x, y, hangerImage.size.width, hangerImage.size.height) tag:5363+i];
        [self addCategoryTitle:@"地獄" frame:CGRectMake(x, y+hangerImage.size.height, hangerImage.size.width, 15) tag:434+i];
    }
    
    int contentHeight=(categoryImagesArray.count%3!=0)?(categoryImagesArray.count/3)+1:categoryImagesArray.count/3;
    [scrolView setContentSize:CGSizeMake(scrolView.frame.size.width, contentHeight*110+8)];
    
    
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(213, 162, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
}


-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}

-(void)addCategoryImage:(UIImage*)image frame:(CGRect)frame  tag:(int)tag
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    [imageView setImage:image];
    [imageView setTag:tag];
    [scrolView addSubview:imageView];
}

-(void)addCategoryTitle:(NSString*)title frame:(CGRect)frame  tag:(int)tag
{
    UILabel *lable=[[UILabel alloc]initWithFrame:frame];
    [lable setText:title];
    [lable setFont:[UIFont systemFontOfSize:11.0]];
    [lable setTextColor:[UIColor colorWithRed:151/255.0 green:107/255.0 blue:79/255.0 alpha:1.0f]];
    [lable setTextAlignment:NSTextAlignmentCenter];
    [lable setTag:tag];
    [lable setBackgroundColor:[UIColor clearColor]];
    [scrolView addSubview:lable];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [self.view addSubview:button];

    //buttonimg.layer.borderColor=[[UIColor blackColor]CGColor];

}

-(IBAction)button:(UIButton*)sender
{
    if (sender.tag == 1012)
    {
        CoordiViewController *coordi_home = (CoordiViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        [self.navigationController popToViewController:coordi_home animated:YES];
    }
}



@end
