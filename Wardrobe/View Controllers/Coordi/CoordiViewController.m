//
//  CoordiViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "CoordiViewController.h"
#import "HomeViewController.h"
#import "Coordi_Calender.h"
#import "Coordi_Category.h"
#import "Coordi_Coordi.h"
#import "Coordi_Taste.h"
#import "Coordi_BrandShop.h"
#import "HomeViewController.h"
#import "CoordiViewController.h"
#import "SNSViewController.h"
#import "NewsViewController.h"
#import "SettingsViewController.h"

@interface CoordiViewController ()

@end

@implementation CoordiViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *calenderImg=[UIImage imageNamed:@"circle_light"];
    UIImage *darkImg=[UIImage imageNamed:@"circle_dark"];
    [self addButtonWithFrame:CGRectMake(50.0, 82.0, calenderImg.size.width  , calenderImg.size.height) tag:1008 title:@"Calender" image:calenderImg];
    [self addButtonWithFrame:CGRectMake(165.0, 82.0, calenderImg.size.width  , calenderImg.size.height) tag:1009 title:@"Category" image:calenderImg];
    [self addButtonWithFrame:CGRectMake(108.0, 177.0, darkImg.size.width  , darkImg.size.height) tag:1010 title:@"Coordi" image:darkImg];
    [self addButtonWithFrame:CGRectMake(50.0, 270.0, calenderImg.size.width  , calenderImg.size.height) tag:1011 title:@"Taste" image:calenderImg];
    [self addButtonWithFrame:CGRectMake(165.0, 270.0, calenderImg.size.width  , calenderImg.size.height) tag:1012 title:@"" image:[UIImage imageNamed:@"circle_line"]];
    
    
    [self addLabelWithFrame:CGRectMake(197.0, 300, 60.0, 20.0) tag:2001 title:@"Brand"];
    [self addLabelWithFrame:CGRectMake(200.0, 333, 60.0, 20.0) tag:2001 title:@"Shop"];
//    
//    [self addButtonWithFrame:CGRectMake(50.0, 64.0, 106.0, 106.0) tag:1008 title:[NSString stringWithFormat:NSLocalizedString(@"Calender", nil)] image:[UIImage imageNamed:@"circle_light"]];
//    [self addButtonWithFrame:CGRectMake(165.0, 64.0, 106.0, 106.0) tag:1009 title:[NSString stringWithFormat:NSLocalizedString(@"Category", nil)] image:[UIImage imageNamed:@"circle_light"]];
//    [self addButtonWithFrame:CGRectMake(108.0, 160.0, 106.0, 106.0) tag:1010 title:[NSString stringWithFormat:NSLocalizedString(@"Coordi", nil)] image:[UIImage imageNamed:@"circle_dark"]];
//    [self addButtonWithFrame:CGRectMake(50.0, 252.0, 106.0, 106.0) tag:1011 title:[NSString stringWithFormat:NSLocalizedString(@"Taste", nil)] image:[UIImage imageNamed:@"circle_light"]];
//    [self addButtonWithFrame:CGRectMake(165.0, 252.0, 106.0, 106.0) tag:1012 title:@"" image:[UIImage imageNamed:@"circle_line"]];
//
//    [self addLabelWithFrame:CGRectMake(197.0, 285.0, 60.0, 20.0) tag:2001 title:[NSString stringWithFormat:NSLocalizedString(@"Brand", nil)]];
//    [self addLabelWithFrame:CGRectMake(200.0, 310.0, 60.0, 20.0) tag:2001 title:[NSString stringWithFormat:NSLocalizedString(@"Shop", nil)]];
        // Do any additional setup after loading the view.
    
    
    
    // Do any additional setup after loading the view.

}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    
    [self.view addSubview:button];
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    label.font = [UIFont fontWithName:@"Times New Roman" size:16];
    [label setTextColor: [UIColor blackColor]];
    [self.view addSubview: label];
}

-(IBAction)button:(UIButton *)sender
{
    if (sender.tag ==1001)
    {
        
    }
    if (sender.tag == 1002)
    {
        CoordiViewController * coordi = [[CoordiViewController alloc]init];
        [self.navigationController pushViewController:coordi animated:NO];
    }
    if (sender.tag == 1003)
    {
        SNSViewController * snsview = [[SNSViewController alloc]init];
        [self.navigationController pushViewController:snsview animated:NO];
    }
    if (sender.tag == 1004)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    if (sender.tag == 1005)
    {
        SettingsViewController * setting = [[SettingsViewController alloc]init];
        [self.navigationController pushViewController:setting animated:NO];
    }
    if (sender.tag == 1007)
    {
        HomeViewController *home = (HomeViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:home animated:YES];
    }
    if (sender.tag == 1008)
    {
        Coordi_Calender * calender = [[Coordi_Calender alloc]init];
        [self.navigationController pushViewController:calender animated:YES];
    }
    if (sender.tag == 1009)
    {
        Coordi_Category * category = [[Coordi_Category alloc]init];
        [self.navigationController pushViewController:category animated:YES];
    }
    if (sender.tag == 1010)
    {
        Coordi_Coordi * coordi = [[Coordi_Coordi alloc]init];
        [self.navigationController pushViewController:coordi animated:YES];
    }
    if (sender.tag == 1011)
    {
        Coordi_Taste * taste = [[Coordi_Taste alloc]init];
        [self.navigationController pushViewController:taste animated:YES];
    }
    if (sender.tag == 1012)
    {
        Coordi_BrandShop * brandshop = [[Coordi_BrandShop alloc]init];
        [self.navigationController pushViewController:brandshop animated:YES];
    }
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
