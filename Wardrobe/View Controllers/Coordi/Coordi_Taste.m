//
//  Coordi_Taste.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 17/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Coordi_Taste.h"
#import "CoordiViewController.h"

@interface Coordi_Taste ()
{
    NSMutableArray * image_array;
    UIScrollView * scroller;
}

@end

@implementation Coordi_Taste

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"home_background"]]];
    image_array = [[NSMutableArray alloc]initWithObjects:@"compass",@"flag",@"torch",@"bottle",@"tag1",@"headphone",@"taaj",@"skates",@"racket",@"shoes",@"icon11",@"tie",@"rupees",@"tag",@"hanger",@"tank",@"mode",@"guitar",@"ribbon", nil];
    
    scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 70.0, 315.0, 320.0)];
    scroller.delegate = (id)self;
    scroller.showsVerticalScrollIndicator = YES;
    scroller.scrollEnabled = YES;
    scroller.userInteractionEnabled =YES;
    
    int contentHeight=(image_array.count%3!=0)?(image_array.count/3)+1:image_array.count/3;
    [scroller setContentSize:CGSizeMake(scroller.frame.size.width, contentHeight*110+40)];
    
    //scroller.contentSize =CGSizeMake(0.0, 1000.0);
    
    scroller.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scroller];
    
    [self addButtonImages];
    
    
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(218.0, 170.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addButtonImages
{
    float horizontal = 9.0;
    float vertical = 10.0;
    UILabel * lblimages;
    
    for (int i =0; i<[image_array count]; i++)
    {
        if ((i%3) ==0 && i!=0)
        {
            horizontal = 9.0;
            vertical = vertical + 80.0 + 37.0;
        }
        UIButton * buttonimages = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonimages setFrame:CGRectMake(horizontal, vertical, 77.0, 77.0)];
        [buttonimages setTag:i];
        [buttonimages setImage:[UIImage imageNamed:[image_array objectAtIndex:i]] forState:UIControlStateNormal];
        
        [buttonimages addTarget:self action:@selector(button:) forControlEvents:UIControlEventTouchUpInside];
        
        
        lblimages = [[UILabel alloc]init];
        [lblimages setFrame:CGRectMake(horizontal+ 4.0, vertical+ 84.0, 67.0, 17.0)];
        [lblimages setTag:i];
        [lblimages setTextAlignment:NSTextAlignmentCenter];
        [lblimages setText:@"Dress"];
        [lblimages setTextColor:[UIColor colorWithRed:181.0/255 green:155.0/255 blue:126.0/255 alpha:1.0]];
        lblimages.font = [UIFont fontWithName:@"Times New Roman" size:13];
        [lblimages setBackgroundColor:[UIColor clearColor]];
        
        [scroller addSubview:buttonimages];
        [scroller addSubview:lblimages];
        horizontal = horizontal + 80.0 + 30.0;
    }
}

-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    //button.layer.borderColor=[[UIColor blackColor]CGColor];
    //button.layer.borderWidth= 1.0f;
}

-(IBAction)button:(UIButton*)sender
{
    if (sender.tag == 1012)
    {
        CoordiViewController *coordi_home = (CoordiViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        [self.navigationController popToViewController:coordi_home animated:YES];
    }
}

@end
