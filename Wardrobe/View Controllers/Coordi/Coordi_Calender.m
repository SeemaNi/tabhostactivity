//
//  Coordi_Calender.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 17/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "Coordi_Calender.h"
#import "CoordiViewController.h"
#import "LBorderView.h"

@interface Coordi_Calender ()
{
    UIScrollView * calanderscroller;
    NSArray * image_array;
    UIView * gesturesubview;
    UIView * gestureview;
}

@end

@implementation Coordi_Calender

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addViewContents];
    [self addScrollViewWithFrame:CGRectMake(2.0, 140.0, 317.0, 250.0)];
    [self addLabelWithFrame:CGRectMake(103.0, 10.0, 123.0, 17.0) color:[UIColor blackColor] font:[UIFont fontWithName:@"Helvetica-Bold" size:16] title:@"2014 December"];
    [self addButtonWithFrame:CGRectMake(63.0, 07.0, 24.0, 24.0) tag:1001 title:@"" image:[UIImage imageNamed:@"prev"]];
    [self addButtonWithFrame:CGRectMake(237.0, 07.0, 24.0, 24.0) tag:1002 title:@"" image:[UIImage imageNamed:@"next"]];
    [self addView];
    image_array = [[NSArray alloc]initWithObjects:@"chetan",@"Ruchi",@"Swati",@"chetan",@"Ruchi",@"calender_5.jpg", nil];
    UIImage *sliderBGImage=[UIImage imageNamed:@"slider_bg"];
    UIImage *thumbImage=[UIImage imageNamed:@"slider_tracker"];
    [self addSliderWithFrame:CGRectMake(218.0, 227.0, sliderBGImage.size.width, sliderBGImage.size.height) BGImage:sliderBGImage trackerImg:thumbImage];
    //[self addImageView];
    
    
//    [self addImageViewWithFrame:CGRectMake(10.0, 36.0, 63.0, 63.0) image:[UIImage imageNamed:@"calender_5.jpg"]];
//    [self addImageViewWithFrame:CGRectMake(15.0, 41.0, 63.0, 63.0) image:[UIImage imageNamed:@"calender_5.jpg"]];
//    [self addImageViewWithFrame:CGRectMake(19.0, 45.0, 63.0, 63.0) image:[UIImage imageNamed:@"calender_5.jpg"]];
    
}

-(void)addSliderWithFrame:(CGRect)frame BGImage:(UIImage*)image trackerImg:(UIImage*)trackerImage
{
    UISlider *slider=[[UISlider alloc]initWithFrame:frame];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    slider.transform = trans;
    [slider setThumbImage:trackerImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:image forState:UIControlStateNormal];
    [slider setMinimumTrackImage:image forState:UIControlStateNormal];
    [self.view addSubview:slider];
}


-(void)addView
{
    float horizontal = 10.0;
    float vertical = 36.0;
    UILabel * lbl_no;
    
    for(int i=0; i<31 ; i++)
    {
        if((i%4) == 0 && i!=0)
        {
            horizontal = 10.0;
            vertical = vertical + 57.0 + 22.0;
        }
        if (i!=4 && i!=5 && i!=6 && i!=14)
        {
            _borderView4 = [[LBorderView alloc]initWithFrame:CGRectMake(horizontal+1, vertical+1, 63.0, 63.0)];
            _borderView4.borderType = BorderTypeDashed;
            _borderView4.dashPattern = 4;
            _borderView4.spacePattern = 4;
            _borderView4.borderWidth = 1;
            _borderView4.cornerRadius = 05;
        }
        if (i==4 || i==5 ||i==6 ||i==14)
        {
            UIView * view = [[UIView alloc]init];
            view.frame =CGRectMake(horizontal, vertical, 58.0, 63.0);
            view.layer.backgroundColor = [UIColor colorWithRed:249.0/255 green:242.0/255 blue:247.0/255 alpha:1.0].CGColor;
            view.layer.borderColor = [UIColor colorWithRed:210.0/255 green:200.0/255 blue:191.0/255 alpha:1.0].CGColor;
            view.layer.borderWidth = 1.0f;
            [calanderscroller addSubview:view];
            
            UIView * view1 = [[UIView alloc]init];
            view1.frame =CGRectMake(horizontal+3, vertical+3, 58.0, 63.0);
            view1.layer.backgroundColor = [UIColor colorWithRed:249.0/255 green:242.0/255 blue:247.0/255 alpha:1.0].CGColor;
            view1.layer.borderColor = [UIColor colorWithRed:210.0/255 green:200.0/255 blue:191.0/255 alpha:1.0].CGColor;
            view1.layer.borderWidth = 1.0f;
            [calanderscroller addSubview:view1];
            
            UIView * view2 = [[UIView alloc]init];
            view2.frame =CGRectMake(horizontal+6, vertical+6, 58.0, 63.0);
            view2.layer.backgroundColor = [UIColor colorWithRed:249.0/255 green:242.0/255 blue:247.0/255 alpha:1.0].CGColor;
            view2.layer.borderColor = [UIColor colorWithRed:210.0/255 green:200.0/255 blue:191.0/255 alpha:1.0].CGColor;
            view2.layer.borderWidth = 1.0f;
            [calanderscroller addSubview:view2];
            
            UILabel *lbl2 = [[UILabel alloc]init];
            lbl2.frame = CGRectMake(horizontal+07, vertical+56, 58.0, 10.0);
            lbl2.textAlignment = NSTextAlignmentCenter;
            lbl2.textColor = [UIColor colorWithRed:143.0/255 green:137.0/255 blue:137.0/255 alpha:1.0] ;
            lbl2.text = @"Dress";
            lbl2.font = [UIFont fontWithName:@"Times New Roman" size:07];
            [calanderscroller addSubview:lbl2];
            
            UIImageView *image_view = [[UIImageView alloc]init];
            image_view.frame = CGRectMake(horizontal+10, vertical+9, 50.0, 45.0);
            image_view.image = [UIImage imageNamed:@"calender_5.jpg"];
            [calanderscroller addSubview:image_view];
            
            UILabel *lbl1 = [[UILabel alloc]init];
            lbl1.frame = CGRectMake(horizontal+28, vertical+39, 16.0, 16.0);
            lbl1.layer.borderColor = [UIColor colorWithRed:86.0/255 green:59.0/255 blue:13.0/255 alpha:1.0].CGColor;
            lbl1.textAlignment = NSTextAlignmentCenter;
            lbl1.textColor = [UIColor colorWithRed:249.0/255 green:240.0/255 blue:236.0/255 alpha:1.0] ;
            lbl1.text = [NSString stringWithFormat:@"%d",i+1];
            lbl1.backgroundColor  = [UIColor colorWithRed:86.0/255 green:59.0/255 blue:13.0/255 alpha:1.0];
            lbl1.font = [UIFont fontWithName:@"Cooper Std" size:12];
            lbl1.layer.cornerRadius = 07.0;
            [lbl1 setClipsToBounds:YES];
            [calanderscroller addSubview:lbl1];
            
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOnItemSelection:)];
            [tapGesture setDelegate:(id)self];
            [calanderscroller addGestureRecognizer:tapGesture];
            
        }

        
//            UIView * view = [[UIView alloc]initWithFrame:CGRectMake(horizontal, vertical, 63.0, 63.0)];
//            view.backgroundColor = [UIColor clearColor] ;
//            [calanderscroller addSubview:view];
            [calanderscroller addSubview:_borderView4];
        if (i!=5 && i!=6 && i!=4 && i!=14)
        {
            lbl_no = [[UILabel alloc]init];
            [lbl_no setFrame:CGRectMake(horizontal, vertical, 13.0, 13.0)];
            lbl_no.textColor =[UIColor colorWithRed:138.0/255 green:103.0/255 blue:61.0/255 alpha:1.0];
            lbl_no.text = [NSString stringWithFormat:@"%d",i+1];
            lbl_no.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
            [lbl_no setBackgroundColor:[UIColor colorWithRed:252.0/255 green:238.0/255 blue:222.0/255 alpha:1.0]];
            [calanderscroller addSubview:lbl_no];
        }
            horizontal = horizontal + 65.0 + 12.0;
    }
}

-(IBAction)actionOnItemSelection:(UIGestureRecognizer*)sender
{
    gestureview = [[UIView alloc]init];
    gestureview.frame = CGRectMake(0.0, 137.0, 320.0,300.0 );
    gestureview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"clothe"]];
    [self.view addSubview:gestureview];
    
    gesturesubview = [[UIView alloc]init];
    gesturesubview.frame = CGRectMake(1.0, 188.0, 318.0, 162.0);
    gesturesubview.backgroundColor = [UIColor colorWithRed:244.0/255 green:222.0/255 blue:203.0/255 alpha:0.5];
    gesturesubview.layer.borderColor = [UIColor colorWithRed:253.0/255 green:246.0/255 blue:239.0/255 alpha:1.0].CGColor;
    gesturesubview.layer.borderWidth = 3.5;
    //[gesturesubview setOpaque:YES];
    [self.view addSubview:gesturesubview];
    
//    UILabel * lbl = [[UILabel alloc]init];
//    lbl.frame = CGRectMake(10.0, 26.0, 150.0, 10.0);
//    lbl.textColor = [UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0];
//    lbl.text = @"chetan";
//    lbl.backgroundColor = [UIColor blackColor];
//    [gesturesubview addSubview:lbl];
   
    [self addLabelWithFrame:CGRectMake(28.0, 12.0, 80.0, 20.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"Coordi"];
    [self addLabelWithFrame:CGRectMake(28.0, 48.0, 80.0, 20.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"Coordi"];
    [self addLabelWithFrame:CGRectMake(28.0, 83.0, 80.0, 20.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"Coordi"];
    [self addLabelWithFrame:CGRectMake(28.0, 120.0, 80.0, 20.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"Coordi"];


    [self addLabelWithFrame:CGRectMake(12.0, 39.0, 300.0, 2.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"-------------------------------------------------------------------------"];
    
    [self addLabelWithFrame:CGRectMake(12.0, 77.0, 300.0, 2.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"-------------------------------------------------------------------------"];
    
    [self addLabelWithFrame:CGRectMake(12.0, 114.0, 300.0, 2.0) color:[UIColor colorWithRed:88.0/255 green:54.0/255 blue:26.0/255 alpha:1.0] font:[UIFont fontWithName:@"Times New Roman" size:12] title:@"-------------------------------------------------------------------------"];
    
    [self addButtonWithFrame:CGRectMake(08.0, 224.0, 70.0, 22.0) tag:1003 title:@"Submit" image:[UIImage imageNamed:@"bigbtn"]];
    [self addButtonWithFrame:CGRectMake(124.0, 224.0, 70.0, 22.0) tag:1003 title:@"" image:[UIImage imageNamed:@"okbtn"]];
    [self addButtonWithFrame:CGRectMake(242.0, 224.0, 70.0, 22.0) tag:1003 title:@"Done" image:[UIImage imageNamed:@"bigbtn"]];
    
}

-(void)addViewContents
{
    UIScrollView *scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height)];
    [scrollView setShowsVerticalScrollIndicator:NO];
    [scrollView setDelegate:(id)self];
    [scrollView setTag:2000];
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scrollView];

    
    //####### Top View ##########
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(8, 8, 52, 52)];
    [tempView setBackgroundColor:[UIColor colorWithRed:210/255.0 green:204/255.0 blue:200/255.0 alpha:1.0]];
    [scrollView addSubview:tempView];
    UIImageView *profileImg=[[UIImageView alloc]initWithFrame:CGRectMake(1, 1, 50, 50)];
    [profileImg setTag:2001];
    [profileImg setImage:[UIImage imageNamed:@"chen"]];
    [profileImg.layer setBorderColor:[UIColor whiteColor].CGColor];
    [profileImg.layer setBorderWidth:2.0f];
    [tempView addSubview:profileImg];
    UIColor *clearColor=[UIColor clearColor];
    [self addLabelWithFrame:CGRectMake(75, 7, 190, 10) tag:2002 title:@"Yamada2013" textColor:[UIColor colorWithRed:149/255.0 green:77/255.0 blue:38/255.0 alpha:1.0] font:[UIFont systemFontOfSize:11] attributedTitle:nil bgColor:clearColor];
    NSString *cordiCount=@"300";
    NSString *followCount=@"10";
    NSString *followedCount=@"100";
    NSMutableAttributedString *cordiStr=[[NSMutableAttributedString alloc]initWithString:@"coordi 300    follow 10   followe 100"];
    
    UIColor *numberColor=[UIColor colorWithRed:0 green:81/255.0 blue:105/255.0 alpha:1.0];
    [cordiStr addAttribute:NSFontAttributeName  value:[UIFont boldSystemFontOfSize:15] range:[[cordiStr string]rangeOfString:cordiCount]];
    [cordiStr addAttribute:NSForegroundColorAttributeName  value:numberColor range:[[cordiStr string]rangeOfString:cordiCount]];
    
    [cordiStr addAttribute:NSFontAttributeName  value:[UIFont boldSystemFontOfSize:15] range:[[cordiStr string]rangeOfString:followCount]];
    [cordiStr addAttribute:NSForegroundColorAttributeName  value:numberColor range:[[cordiStr string]rangeOfString:followCount]];
    [cordiStr addAttribute:NSFontAttributeName  value:[UIFont boldSystemFontOfSize:15] range:[[cordiStr string]rangeOfString:followedCount]];
    [cordiStr addAttribute:NSForegroundColorAttributeName  value:numberColor range:[[cordiStr string]rangeOfString:followedCount]];
    [self addLabelWithFrame:CGRectMake(75, 25, 240, 14) tag:2003 title:nil textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:12] attributedTitle:cordiStr bgColor:clearColor];
    [self addLabelWithFrame:CGRectMake(0, 73, self.view.frame.size.width, 1) tag:11 title:nil textColor:nil font:nil attributedTitle:nil bgColor:[UIColor colorWithRed:229/255.0 green:208/255.0 blue:195/255.0 alpha:1.0]];
}

-(void)addBorderViewWithFrame:(CGRect)frame bgcolor:(UIColor *)bgcolor border:(UIColor * )bordercolor
{
    UIView * borderview = [[UIView alloc]init];
    borderview.frame = frame;
    borderview.backgroundColor = bgcolor;
    [borderview.layer setBackgroundColor:bordercolor.CGColor];
    [calanderscroller addSubview:borderview];
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title textColor:(UIColor*)textColor font:(UIFont*)font attributedTitle:(NSMutableAttributedString*)attTitle bgColor:(UIColor*)bgColor
{
    UILabel *lbl=[[UILabel alloc]initWithFrame:frame];
    [lbl setTag:tag];
    [lbl setText:title];
    [lbl setBackgroundColor:bgColor];
    [lbl setTextColor:textColor];
    [lbl setFont:font];
    if (attTitle)
    {
        [lbl setAttributedText:attTitle];
    }
    [[self getScrollView]addSubview:lbl];
}

-(UIScrollView*)getScrollView
{
    UIScrollView *scrollView=(UIScrollView*)[self.view viewWithTag:2000];
    return  scrollView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

-(void)addScrollViewWithFrame:(CGRect)frame
{
    calanderscroller = [[UIScrollView alloc]init];
    calanderscroller.frame = frame;
    calanderscroller.delegate = (id)self ;
    calanderscroller .userInteractionEnabled = YES;
    calanderscroller.showsVerticalScrollIndicator = YES;
    [calanderscroller setBackgroundColor:[UIColor clearColor]];
    [calanderscroller setContentSize:CGSizeMake(0.0, 670.0)];
    [self.view addSubview:calanderscroller];
}

//-(void)addLabelForGestureWithFrame:(CGRect)frame color:(UIColor *)color font:(UIFont *)font title:(NSString *)title
//{
//    UILabel *label = [[UILabel alloc]init];
//    label.frame = frame;
//    label.textColor = color;
//    label.font = font;
//    label.text = title;
//    [label setBackgroundColor:[UIColor redColor]];
//    [gesturesubview addSubview:label];
//}

-(void)addLabelWithFrame:(CGRect)frame color:(UIColor *)color font:(UIFont *)font title:(NSString *)title
{
    UILabel *label = [[UILabel alloc]init];
    label.frame = frame;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = color;
    label.font = font;
    label.text = title;
    [calanderscroller addSubview:label];
    [gesturesubview addSubview:label];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(button:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [button setTitleColor:[UIColor colorWithRed:153.0/255 green:116.0/255 blue:61.0/255 alpha:1.0] forState:UIControlStateNormal];
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    //[self.view addSubview:button];
    [calanderscroller addSubview:button];
    [gestureview addSubview:button];
}
-(void)addImageView
{
    float horizontal = 10.0;
    float vertical = 36.0;
   // UILabel * lbl_no;
    
    for(int i=0; i<[image_array count]; i++)
    {
        if((i%4) == 0 && i!=0)
        {
            horizontal = 10.0;
            vertical = vertical + 57.0 + 22.0;
        }

        UIImageView * imageview = [[UIImageView alloc]init];
        imageview.frame = CGRectMake(horizontal, vertical, 63.0, 63.0);
        //imageview.image = image;
        imageview.layer.borderColor = [UIColor blackColor].CGColor;
        [calanderscroller addSubview:imageview];
        
        [imageview setImage:[UIImage imageNamed:[image_array objectAtIndex:i]]];
        
        UIView * imgview_border = [[UIView alloc]init];
        imgview_border.frame = CGRectMake(horizontal+2, vertical+2, 63.0, 63.0);
        imageview.backgroundColor = [UIColor colorWithRed:246.0/255 green:241.0/255 blue:245.0/255 alpha:1.0];
        [calanderscroller addSubview:imgview_border];
       // horizontal = horizontal + 65.0 + 12.0;
    }
}

-(IBAction)button:(UIButton*)sender
{
    if (sender.tag == 1012)
    {
        CoordiViewController *coordi_home = (CoordiViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        [self.navigationController popToViewController:coordi_home animated:YES];
    }
}

@end
