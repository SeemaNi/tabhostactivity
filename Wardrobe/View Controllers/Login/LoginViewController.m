//
//  LoginViewController.m
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "LeveyTabBarController.h"
#import "CameraViewController.h"
#import "SettingsViewController.h"
#import "SNSViewController.h"
#import "CoordiViewController.h"
#import "NewsViewController.h"
#import "AppDelegate.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"loginhome"]]];
    [self addLabelWithFrame:CGRectMake(16.0, 168.0, 80.0, 15.0) tag:1001 title:[NSString stringWithFormat:NSLocalizedString(@"user", nil)]];
    [self addLabelWithFrame:CGRectMake(16.0, 198.0, 80.0, 17.0) tag:1002 title:[NSString stringWithFormat:NSLocalizedString(@"password", nil)]];
    
    [self addTextFieldWithFrame:CGRectMake(104.0, 170.0, 180.0, 15.0) tag:2001 title:@""];
    [self addTextFieldWithFrame:CGRectMake(104.0, 200.0, 180.0, 15.0) tag:2002 title:@""];
    
    [self addButtonWithFrame:CGRectMake(128.0, 228.0, 65.0, 20.0) tag:3001 title:[NSString stringWithFormat:NSLocalizedString(@"login", nil)] image:[UIImage imageNamed:@"loginbutton"]];
    
   
    
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    label.font = [UIFont fontWithName:@"Times New Roman" size:14];
    [label setTextColor: [UIColor colorWithRed:252.0/255 green:238.0/255 blue:214.0/255 alpha:4.0]];
    label.textAlignment = NSTextAlignmentRight;
    [self.view addSubview: label];
}

-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:12];
    tf.backgroundColor=[UIColor whiteColor];
    [tf.layer setCornerRadius:6.50];
   // tf.secureTextEntry = YES;
    tf.tag=tag;
    tf.layer.borderColor=[[UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
    [self.view addSubview:tf];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"Times New Roman" size:12];
    button.frame = frame;
    
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:142.0/255 green:109.0/255 blue:55.0/255 alpha:1.0] forState:UIControlStateNormal];
    [self.view addSubview:button];
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)buttonAction:(id)sender
{
    
    NewsViewController*newsC=[NewsViewController new];
    SNSViewController *snsC=[SNSViewController new];
    SettingsViewController *settingC=[SettingsViewController new];
    CoordiViewController *coordiC=[CoordiViewController new];
    CameraViewController *cameraC=[CameraViewController new];
    
    UINavigationController *newsNaviC=[[UINavigationController alloc]initWithRootViewController:newsC];
    UINavigationController *snsNaviC=[[UINavigationController alloc]initWithRootViewController:snsC];
    UINavigationController *settingNaviC=[[UINavigationController alloc]initWithRootViewController:settingC];
    UINavigationController *cameraNaviC=[[UINavigationController alloc]initWithRootViewController:cameraC];
    UINavigationController *coordiNaviC=[[UINavigationController alloc]initWithRootViewController:coordiC];
    
    NSMutableDictionary *imgDic = [NSMutableDictionary dictionaryWithCapacity:2];
    [imgDic setObject:[UIImage imageNamed:@"camera_icon"] forKey:@"Default"];
    [imgDic setObject:[UIImage imageNamed:@"cameraSelected"] forKey:@"Selected"];
    NSMutableDictionary *imgDic2 = [NSMutableDictionary dictionaryWithCapacity:2];
    [imgDic2 setObject:[UIImage imageNamed:@"coordi_icon"] forKey:@"Default"];
    [imgDic2 setObject:[UIImage imageNamed:@"coordiSelected"] forKey:@"Selected"];
    NSMutableDictionary *imgDic3 = [NSMutableDictionary dictionaryWithCapacity:2];
    [imgDic3 setObject:[UIImage imageNamed:@"sns_icon"] forKey:@"Default"];
    [imgDic3 setObject:[UIImage imageNamed:@"snsSelected"] forKey:@"Selected"];
    NSMutableDictionary *imgDic4 = [NSMutableDictionary dictionaryWithCapacity:2];
    [imgDic4 setObject:[UIImage imageNamed:@"news_icon"] forKey:@"Default"];
    [imgDic4 setObject:[UIImage imageNamed:@"newsSelected"] forKey:@"Selected"];
    NSMutableDictionary *imgDic5 = [NSMutableDictionary dictionaryWithCapacity:2];
    [imgDic5 setObject:[UIImage imageNamed:@"setting_icon"] forKey:@"Default"];
    [imgDic5 setObject:[UIImage imageNamed:@"settingSelected"] forKey:@"Selected"];
    [self.navigationController setNavigationBarHidden:YES];
    

    LeveyTabBarController *tabController=[[LeveyTabBarController alloc]initWithViewControllers:@[cameraNaviC,coordiNaviC,snsNaviC,newsNaviC,settingNaviC] imageArray:@[imgDic,imgDic2,imgDic3,imgDic4,imgDic5]];
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    app.tabController=tabController;
    [tabController.tabBar setBackgroundImage:[UIImage imageNamed:@"tabBarBG"]];
    tabController.defaultSelectedIndex=1;
    [tabController setTabBarTransparent:YES];
    
    [self.navigationController pushViewController:tabController animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
