//
//  SettingCell.m
//  Wardrobe
//
//  Created by Rohit Kumar on 19/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        UIView *backgroundView=[[UIView alloc]initWithFrame:CGRectMake(4, 10, self.frame.size.width-8, 30)];
        [backgroundView setBackgroundColor:[UIColor clearColor]];
        [backgroundView.layer setBorderWidth:1.0f];
        [backgroundView.layer setBorderColor:[UIColor colorWithRed:227/255.0 green:200/255.0 blue:180/255.0 alpha:1.0f].CGColor];
        [self addSubview:backgroundView];
        UIImage *rightArrow=[UIImage imageNamed:@"right_arrow"];
        UIImage *downArrow=[UIImage imageNamed:@"down_arrow"];
        self.accessoryButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessoryButton setFrame:CGRectMake(285, 7, rightArrow.size.width, rightArrow.size.height)];
        [self.accessoryButton setImage:rightArrow forState:UIControlStateNormal];
        [self.accessoryButton setImage:downArrow forState:UIControlStateSelected];

        [backgroundView addSubview:self.accessoryButton];
        self.titleLable=[[UILabel alloc]initWithFrame:CGRectMake(15, 05, 260, 20)];
        [self.titleLable setFont:[UIFont boldSystemFontOfSize:14.0]];
        [self.titleLable setTextColor:[UIColor colorWithRed:104/255.0 green:56/255.0 blue:24/255.0 alpha:1.0]];
        [backgroundView addSubview:self.titleLable];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
