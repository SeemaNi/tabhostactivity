//
//  SettingsViewController.m
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "SettingsViewController.h"
#import "HomeViewController.h"
#import "CoordiViewController.h"
#import "SNSViewController.h"
#import "NewsViewController.h"


@interface SettingsViewController ()
{
    NSArray *settingArray;
}
@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    settingArray=[NSArray arrayWithObjects:NSLocalizedString(@"Personal Data", nil),NSLocalizedString(@"Image", nil),NSLocalizedString(@"Push", nil),NSLocalizedString(@"Notice History", nil),NSLocalizedString(@"Privacy", nil),NSLocalizedString(@"Contact us", nil) ,NSLocalizedString(@"Logout", nil) ,NSLocalizedString(@"Withdrawl", nil),nil];
    //個人データ設定 ,画像登録設定 ,プッシュ通知機能 ,通知履歴表示 ,プライバシー設定 ,お問合せ ,ログアウト ,退会








    
    UITableView *tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, self.topLogoBar.frame.origin.y+self.topLogoBar.frame.size.height, self.view.frame.size.width, 330)];
    [tableView setDelegate:(id)self];
    [tableView setDataSource:(id)self];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setBackgroundColor:[UIColor clearColor]];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:tableView];
    
}


-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setTitleColor:[UIColor colorWithRed:191.0/255 green:50.0/255 blue:29.0/255 alpha:1.0] forState:UIControlStateNormal];
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag == 1007)
    {
        HomeViewController * home = (HomeViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:home animated:NO];
    }
    if (sender.tag ==1001)
    {
        
    }
    if (sender.tag == 1002)
    {
        CoordiViewController * coordi = [[CoordiViewController alloc]init];
        [self.navigationController pushViewController:coordi animated:NO];
    }
    if (sender.tag == 1003)
    {
        SNSViewController * snsview = [[SNSViewController alloc]init];
        [self.navigationController pushViewController:snsview animated:NO];
    }
    if (sender.tag == 1004)
    {
        NewsViewController * news = [[NewsViewController alloc]init];
        [self.navigationController pushViewController:news animated:NO];
    }
    if (sender.tag == 1005)
    {
        SettingsViewController * setting = [[SettingsViewController alloc]init];
        [self.navigationController pushViewController:setting animated:NO];
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegate And DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row==0?47:37;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return settingArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    SettingCell *cell=(SettingCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[SettingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
       
        
    }
    [cell.titleLable.superview setFrame:CGRectMake(4,(indexPath.row==0)? 10:0, self.view.frame.size.width-8, 30)];
    cell.titleLable.text=[settingArray objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingCell *cell =(SettingCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryButton.selected=!cell.accessoryButton.selected;
    NSLog(@"didSelectRowAtIndexPath");
}
@end
