//
//  NewsCustomCell.m
//  Wardrobe
//
//  Created by Rohit Kumar on 20/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "NewsCustomCell.h"

@implementation NewsCustomCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
        self.thumnailImage =[[UIImageView alloc]initWithFrame:CGRectMake(5, 10.5, 33, 33)];
        [self.thumnailImage.layer setBorderColor:[UIColor  whiteColor].CGColor];
        [self.thumnailImage setImage:[UIImage imageNamed:@"cat.jpeg"]];
        [self.thumnailImage.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.thumnailImage.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.thumnailImage.layer setShadowOpacity:0.8];
        [self.thumnailImage.layer setShadowRadius:3.0];
        [self.thumnailImage.layer setShadowOffset:CGSizeMake(1, 1)];
        [self.thumnailImage.layer setBorderWidth:2.0];
        [self addSubview:self.thumnailImage];
        
        self.headingLable=[[UILabel alloc]initWithFrame:CGRectMake(41, 9, 250, 14)];
        [self.headingLable setTextColor:[UIColor colorWithRed:161/255.0 green:99/255.0 blue:50/255.0 alpha:1.0]];
        [self.headingLable setFont:[UIFont boldSystemFontOfSize:11]];
        [self.headingLable setText:@"Fat rat001∞∞∑√˙Ω≈∫ß∂"];
        [self addSubview:self.headingLable];
        
        self.detailLable=[[UILabel alloc]initWithFrame:CGRectMake(41, 26, 250, 14)];
        [self.detailLable setTextColor:[UIColor blackColor]];
        [self.detailLable setFont:[UIFont systemFontOfSize:13]];

        [self.detailLable setText:@"12 H 1 8 fldjf jfds"];
        
        UIImage *arrowImage=[UIImage imageNamed:@"small_arrow"];
        self.arrowButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [self.arrowButton setFrame:CGRectMake(268, 30, arrowImage.size.width,arrowImage.size.height)];
        [self.arrowButton setImage:arrowImage forState:UIControlStateNormal];
        [self addSubview:self.arrowButton];
        [self addSubview:self.detailLable];
  
        UIImage *buttonImage=[UIImage imageNamed:@"small_button"];
        self.righButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.righButton setFrame:CGRectMake(285, 30, buttonImage.size.width, buttonImage.size.height)];
        [self.righButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
        [self.righButton setTitle:@"Hello" forState:UIControlStateNormal];
        [self.righButton.titleLabel setTextColor:[UIColor blackColor]];
        [self.righButton.titleLabel setFont:[UIFont systemFontOfSize:9.0]];
        self.righButton.tag = 001;
        [self addSubview:self.righButton];
    }
    return self;
}
- (void)awakeFromNib
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
