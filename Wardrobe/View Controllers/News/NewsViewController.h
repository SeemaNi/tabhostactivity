//
//  NewsViewController.h
//  Wardrobe
//
//  Created by Chetan Rajauria on 14/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface NewsViewController : CommonViewController<UITextViewDelegate>

@end
